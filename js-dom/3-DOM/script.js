'use strict';

const body = document.querySelector('body');

const clock = document.createElement('div');

body.append(clock);

setInterval(() => {
  clock.textContent = new Date().toLocaleTimeString('es-Es');
}, 1000);
