/* 
Edita el archivo script.js para crear una función que haga lo siguiente:

- Generar una contraseña (número entero aleatorio del 0 al 100)
- Pedir al usuario que introduzca un número dentro de ese rango.
- Si el número introducido es igual a la contraseña, aparecerá en pantalla un mensaje indicando que ha ganado; si no, el mensaje indicará si la contraseña en un número mayor o menor al introducido y dará una nueva oportunidad.
- El usuario tendrá 5 oportunidades de acertar, si no lo consigue, aparecerá un mensaje indicando que ha perdido. */

'use strict';

const password = Math.floor(Math.random() * 100);

for (let tries = 1; tries < 6; tries++) {
  const guess = Number(prompt(`Introduce un número entre 1 y 100`));
  if (guess === password) {
    alert('Has ganado');
    break;
  } else if (guess > password) {
    alert(`El número es mayor que la contraseña. Intento ${tries} de 5`);
  } else {
    alert(`El número es menor que la contraseña. Intento ${tries} de 5`);
  }
  if (tries === 5) alert('Has perdido');
}
