'use strict';

// puntuaciones
const puntuaciones = [
  {
    equipo: 'Toros Negros',
    puntos: [1, 3, 4, 2, 10, 8],
  },
  {
    equipo: 'Amanecer Dorado',
    puntos: [8, 5, 2, 4, 7, 5, 3],
  },
  {
    equipo: 'Águilas Plateadas',
    puntos: [5, 8, 3, 2, 5, 3],
  },
  {
    equipo: 'Leones Carmesí',
    puntos: [5, 4, 3, 1, 2, 3, 4],
  },
  {
    equipo: 'Rosas Azules',
    puntos: [2, 1, 3, 1, 4, 3, 4],
  },
  {
    equipo: 'Mantis Verdes',
    puntos: [1, 4, 5, 1, 3],
  },
  {
    equipo: 'Ciervos Celestes',
    puntos: [3, 5, 1, 1],
  },
  {
    equipo: 'Pavos Reales Coral',
    puntos: [2, 3, 2, 1, 4, 3],
  },
  {
    equipo: 'Orcas Moradas',
    puntos: [2, 3, 3, 4],
  },
];

const getBlackClover = (puntuaciones) => {
  let winnerScore = puntuaciones[0].puntos.reduce((a, b) => a + b, 0);
  let loserScore = puntuaciones[0].puntos.reduce((a, b) => a + b, 0);
  let loserName = '';
  let winnerName = '';

  for (let i = 0; i < puntuaciones.length; i++) {
    if (puntuaciones[i].puntos.reduce((a, b) => a + b, 0) > winnerScore) {
      winnerScore = puntuaciones[i].puntos.reduce((a, b) => a + b, 0);
      winnerName = puntuaciones[i].equipo;
    }
  }

  for (let i = 0; i < puntuaciones.length; i++) {
    if (puntuaciones[i].puntos.reduce((a, b) => a + b, 0) < loserScore) {
      loserScore = puntuaciones[i].puntos.reduce((a, b) => a + b, 0);
      loserName = puntuaciones[i].equipo;
    }
  }
  return `La orden ganadora es ${winnerName} con ${winnerScore} estrellas y la orden perdedora es ${loserName} con ${loserScore} estrellas.`;
};

console.log(getBlackClover(puntuaciones));
